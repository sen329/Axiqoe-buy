export class Proposal {
    id: number;
    SalesName: string;
    CustomerName: string;
    CustomerAddress: string;
    CustomerContact: number;
    ProductUsedId: number;
    ProductName: string;
    ProductCode:string;
    ProductQuantity: number;
    ProposedPrice: number;
    Margin: number;
    Accepted: boolean;
}
